/**
 * Created by user on 2018/1/16.
 */
import HtmlwebpackPlugin = require('html-webpack-plugin');
import CleanWebpackPlugin = require('clean-webpack-plugin');
import OpenBrowserPlugin = require('open-browser-webpack-plugin');
import AutoDllPlugin = require('autodll-webpack-plugin');
import UglifyJSPlugin = require('uglifyjs-webpack-plugin');
import webpack = require('webpack');
import path = require('path');
import {Utils} from "./Utils";

const src = Utils.resolve('/src');
const dist = Utils.resolve('/dist');

const config: webpack.Configuration = {
    entry: {
        app:src + "/app.js", //已多次提及的唯一入口文件
    },
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.ts?$/,
                use: "babel?presets[]=es2015!ts"
            }
        ]
    },
    output: {
        path: dist,
        filename: "bundle-[hash].js",
        publicPath: "http://www.newagirl.com/"
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlwebpackPlugin({
            inject: true,
            template: src+'/index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeAttributeQuotes: true
            }
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        new AutoDllPlugin({
            inject: true, // will inject the DLL bundles to index.html
            filename: '[name]_[hash].js',
            entry: {
                vendor: [
                    src+'/js/jquery.js',
                    src+'/js/angular.js'
                ]
            },
            plugins: [new UglifyJSPlugin()]
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
};
export {config as webpackConfig};

