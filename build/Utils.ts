/**
 * Created by user on 2018/1/5.
 */
import {Factory} from "./oop/Factory";
import path = require('path');
export class Utils{
    public static extend(obj:any,newObj:any,isDep?:boolean): any {
        if(isDep){
            for(var key in newObj){
                if(Object.prototype.toString.call(newObj[key]) == '[object Array]' || Object.prototype.toString.call(newObj[key]) == '[object Object]'){
                    Utils.extend(obj[key],newObj[key]);
                }else{
                    obj[key] = newObj[key];
                }
            }
            return obj;
        }
        for(var key in newObj){
            obj[key] = newObj[key];
        }
        return obj;
    }
    public static isObject(obj): boolean{
        return Object.prototype.toString.call(obj) == "[object Object]";
    }
    public static isArray(obj): boolean{
        return Object.prototype.toString.call(obj) == "[object Array]";
    }
    public static isFunction(obj): boolean{
        return Object.prototype.toString.call(obj) == "[object Function]";
    }
    public static each(obj,callback): void{
        if(this.isArray(obj)){
            for(let i = 0; i < obj.length; i++){
                callback(obj[i],i);
            }
        }
        if(this.isObject(obj)){
            for(let key in obj){
                callback(obj[key],key);
            }
        }
    }
    public static resolve (dir) {
        return path.join(__dirname, '..', dir)
    }
}
