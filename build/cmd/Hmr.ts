/**
 * Created by user on 2018/1/19.
 */
import {Factory} from "../oop/Factory";
import {Utils} from "../Utils";
import {EhOnInputCmd,EhInit} from "../oop/intface";
import webpackDevServer = require("webpack-dev-server");
import webpack = require('webpack');
const Options = Factory.getIns('Options');
export class Hmr{
    private opts: any = Factory.getIns('Options').getOpts('evn');
    ehOnInputHmr(){
        this.opts.dev = false;
        this.opts.hmr = true;
        this.opts.min = false;
        const webpackConfig = Options.getOpts('webpackConfig');
        const webpackDevServerConfig = Options.getOpts('webpackDevServer');
        Utils.extend(webpackDevServerConfig,{
            hot:true
        });
        webpackDevServer['addDevServerEntrypoints'](
                webpackConfig,
                webpackDevServerConfig
            );
        let compiler = webpack(webpackConfig);
        let server = new webpackDevServer(compiler, webpackDevServerConfig);
        console.log(webpackDevServerConfig);
        server.listen(this.opts.port, 'localhost', () => {
            console.log(`dev server listening on port ${this.opts.port}`);
        });
    }
}
Factory.setIns('Hmr',Hmr);