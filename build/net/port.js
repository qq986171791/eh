/**
 * Created by user on 2018/1/5.
 */
var options = {
    port:{
        list:[]
    }
};
var factory = {};

factory.strategy = {};

factory.insList = {};

factory.getIns = function (ins) {
    return factory.insList[ins] = factory.insList[ins] || new factory.strategy[ins]();
}

factory.strategy.Port = function () {
    var optsKey = 'port';
    var opts = options[optsKey];
    var deps = {
        plugins:{ //插件
            getPort:require('get-port')
        },
        service:{ //服务

        }
    };
    var ins = {
        init:function () {
        },
        getPort:function () {
            var getPort = deps.plugins.getPort;
            return getPort();
        }
    };
    ins.init();
    return ins;
}
var port = factory.getIns('Port')
module.exports = port;