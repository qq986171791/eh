# eh

<h3>a cli for building spa</h3>

<pre>
  root
  |-src
  |-build
    |-bin
      |-eh.js(run)
    |-cmd
      |-Build.ts(product mode)
      |-Dev.ts(develop mode)
      |-Hmr.ts(hot replace mode)
      |-Input.ts(listen cmd)
      |-Serve.js
    |-lib
      |-bootstrap-local.js(compile ts)
    |-net
      |-ip.js(get a local ip)
      |-port.js(get a available port)
    |-oop
      |-Factory.ts
      |-interface.ts
    Option.ts(default global options)
    Utils.ts
    index.js
    main.ts(entry point)
    webpack.config.ts
</pre>
