/**
 * Created by user on 2018/1/16.
 */
import ng from './js/angular.js';
import $ from './js/jquery.js';
import component from './component.js';
import './css/style.css';
if (module.hot) {
    module.hot.accept('./component.js', function() {
        console.log('Accepting the updated printMe module!');
    })
}
component.printMe();
